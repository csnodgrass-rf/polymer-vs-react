﻿import { html, LitElement } from 'lit-element';

customElements.define('sharing-data', class PolymerSharingData extends LitElement {
  static get properties() {
    return {
      firstName: { type: String },
      lastName: { type: String }
    };
  }
  
  constructor() {
    super();
    
    this.firstName = '';
    this.lastName = '';
  }
  
  render() {
    const { firstName, lastName, handleUpdate } = this;
    
    return html`
      <p>${firstName && lastName 
        ? `Hello ${firstName} ${lastName}` 
        : 'Please enter your name.'
      }</p>
      <sharing-data-child
        first-name=${firstName}
        last-name=${lastName}
        @update=${handleUpdate}
      ></sharing-data-child>
    `;
  }
  
  handleUpdate(event) {
    const { firstName, lastName } = event.detail;
    
    this.firstName = firstName;
    this.lastName = lastName;
  }
});

customElements.define('sharing-data-child', class SharingDataChild extends LitElement {
  static get properties() {
    return {
      firstName: { type: String, attribute: 'first-name' },
      lastName: { type: String, attribute: 'last-name' }
    };
  }
  
  render() {
    const { firstName, lastName, handleFirstNameChange, handleLastNameChange } = this;
    
    return html`
      <input 
        type="text" 
        value=${firstName}
        placeholder="First Name" 
        @change=${handleFirstNameChange} 
      />
      <input 
        type="text" 
        value=${lastName}
        placeholder="Last Name" 
        @change=${handleLastNameChange} 
      />
    `;
  }
  
  handleFirstNameChange(event) {
    const { lastName } = this;
    
    this.dispatchEvent(new CustomEvent('update', {
      detail: { 
        firstName: event.currentTarget.value,
        lastName
      }
    }));
  }
  
  handleLastNameChange(event) {
    const { firstName } = this;

    this.dispatchEvent(new CustomEvent('update', {
      detail: {
        firstName,
        lastName: event.currentTarget.value
      }
    }));
  }
});