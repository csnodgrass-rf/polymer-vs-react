﻿import { html, LitElement } from 'lit-element';

customElements.define('my-counter', class Counter extends LitElement {
  static get properties() {
    return {
      count: { type: Number }
    };
  }
  
  constructor() {
    super();
    
    this.count = 0;
  }
  
  render() {
    const { count, increment, reset } = this;
    
    return html`
      <p>Count: ${count}</p>
      <button @click=${increment}>Add</button>
      <button @click=${reset}>Reset</button>
    `;
  }
  
  increment() {
    this.count++;
  }
  
  reset() {
    this.count = 0;
  }
});