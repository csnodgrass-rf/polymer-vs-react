﻿import { LitElement } from 'lit-element';
import { html } from 'lit-html';

customElements.define('hello-world', class HelloWorld extends LitElement {
  render() {
    return html`
      <div>Hello World</div>
    `;
  }
});