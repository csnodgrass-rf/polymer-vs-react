﻿import { html, css, LitElement } from 'lit-element';

customElements.define('hello-world-with-style', class HelloWorldWithStyle extends LitElement {
  static get styles() {
    // language=css
    return css`
      div {
        background: #EEE;
        border: 1px solid #36F;
        padding: 5px 10px;
        font-size: 20px;
        line-height: 1;
      }
    `;
  }
  
  render() {
    return html`
      <div>Hello World</div>
    `;
  }
});