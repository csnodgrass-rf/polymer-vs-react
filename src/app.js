﻿/* global React */

import Views from './views';

const View = ({ page, title, react, polymer }) => <React.Fragment>
  <h1>Polymer</h1>
  {polymer}
  <h1>React</h1>
  {react}
</React.Fragment>

export default class App extends React.PureComponent {
  state = { current: location.hash.slice(1) || Views.HelloWorld };
  
  Link = ({ page, children }) => <li>
    <a onClick={this.changePage} href={`#${page}`}>{children}</a>
  </li>;
  
  render() {
    const { Link } = this;
    const { current } = this.state;
    
    return <React.Fragment>
      <nav>
        <ul>
          {Views.map(({ page, title }) =>
            <Link key={page} page={page}>{title}</Link> 
          )}
        </ul>
      </nav>
      <main>
        <View { ...Views.filter(({ page }) => page === current)[0] } />
      </main>
    </React.Fragment>;
  }
  
  changePage = (event) => {
    event.preventDefault();
    
    const page = event.currentTarget.getAttribute('href').slice(1);
    
    this.setState({ current: page });
    
    history.pushState('', '', `#${page}`);
  }
}