﻿/* global React */

import './polymer/counter';
import './polymer/hello-world';
import './polymer/hello-world-with-style';
import './polymer/sharing-data';
import Counter from './react/counter';
import HelloWorld from './react/hello-world';
import HelloWorldWithStyle from './react/hello-world-with-style';
import SharingData from './react/sharing-data';

const Views = [
  { 
    page: 'hello-world', 
    title: 'Hello World', 
    polymer: <hello-world/>, 
    react: <HelloWorld/> 
  },
  { 
    page: 'hello-world-with-style', 
    title: 'Hello World With Style', 
    polymer: <hello-world-with-style/>, 
    react: <HelloWorldWithStyle/> 
  },
  {
    page: 'counter',
    title: 'Counter',
    polymer: <my-counter/>,
    react: <Counter/>
  },
  {
    page: 'sharing-data',
    title: 'Sharing Data',
    polymer: <sharing-data/>,
    react: <SharingData/>
  }
];

export default Views;