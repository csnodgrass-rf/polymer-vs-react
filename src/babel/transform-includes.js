﻿const { existsSync } = require('fs');
const { join, dirname } = require('path');

const isProduction = process.env.NODE_ENV === 'production';

const isRelativePattern = /^\./;
const splitPathPattern = /^([^\/]+)(?:\/(.*))?$/;
const endsWithExtensionPattern = /\.[a-z0-9]+$/i;

const resolvePath = (sourcePath, file) => {
  if (!['fs', 'path'].includes(sourcePath) && !isRelativePattern.test(sourcePath)) {
    const [, packageName, path] = sourcePath.match(splitPathPattern);
    const pathWithExtension = path && (endsWithExtensionPattern.test(path) ? path : `${path}.js`);

    try {
      const packageJson = require(`${packageName}/package.json`);

      return `/node_modules/${packageName}/${pathWithExtension || packageJson.module || packageJson.main || 'index.js'}`;
    } catch (_) {
      console.warn(
        `Unable to read package: ${packageName}`,
        { sourcePath, path, pathWithExtension}
      );
    }
  } else if (!endsWithExtensionPattern.test(sourcePath)) {
    if (existsSync(join(dirname(file), `${sourcePath}.js`))) {
      return `${ sourcePath }.js`;
    }
    
    return `${sourcePath}/index.js`;
  }
};

module.exports = { resolvePath };