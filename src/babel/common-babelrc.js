﻿const { resolvePath } = require('../babel/transform-includes');

const presets = [
  [
    '@babel/preset-env', {
    modules: false,
    targets: {
      esmodules: true
    }
  }
  ]
];

const plugins = [
  [
    'babel-plugin-module-resolver', {
      root: ['./src'],
      resolvePath
    }
  ],
  '@babel/plugin-proposal-class-properties'
];

module.exports = {
  sourceMaps: 'inline',
  presets,
  plugins
};