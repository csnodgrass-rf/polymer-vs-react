﻿const common = require('../babel/common-babelrc');

const presets = [ ...common.presets, '@babel/preset-react' ];

module.exports = { ...common, presets };