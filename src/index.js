﻿/* global React, ReactDOM */
import App from './app';

ReactDOM.render(
  <App />, 
  document.querySelector('#root')
);