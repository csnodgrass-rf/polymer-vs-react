﻿/* global React */

export default class SharingData extends React.PureComponent {
  state = { firstName: '', lastName: '' };
  
  render() {
    const { handleUpdate } = this;
    const { firstName, lastName } = this.state;
    
    return <React.Fragment>
      <p>{firstName && lastName ? `Hello ${firstName} ${lastName}` : 'Please enter your name.'}</p>
      <ReactSharingDataChild
        firstName={firstName}
        lastName={lastName}
        onUpdate={handleUpdate}
      />
    </React.Fragment>;
  }
  
  handleUpdate = (firstName, lastName) => {
    this.setState({ firstName, lastName });
  };
}

class ReactSharingDataChild extends React.PureComponent {
  render() {
    const { handleFirstNameChange, handleLastNameChange } = this;
    const { firstName, lastName } = this.props;
    
    return <React.Fragment>
      <input 
        type="text" 
        value={firstName} 
        placeholder="First Name" 
        onChange={handleFirstNameChange}
      />
      <input 
        type="text" 
        value={lastName} 
        placeholder="Last Name" 
        onChange={handleLastNameChange} 
      />
    </React.Fragment>;
  }
  
  handleFirstNameChange = event => {
    const { onUpdate, lastName } = this.props;
    
    onUpdate(event.currentTarget.value, lastName);
  };
  
  handleLastNameChange = event => {
    const { onUpdate, firstName } = this.props;
    
    onUpdate(firstName, event.currentTarget.value);
  };
}