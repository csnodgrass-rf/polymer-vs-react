﻿/* global React */

export default class Counter extends React.PureComponent {
  state = { count: 0 };
  
  render() {
    const { increment, reset } = this;
    const { count } = this.state;
    
    return <React.Fragment>
      <p>Count: {count}</p>
      <button onClick={increment}>Add</button>
      <button onClick={reset}>Reset</button>
    </React.Fragment>
  }
  
  // Note using class-properties because it won't be bound otherwise.
  increment = () => {
    const { count } = this.state;
    
    this.setState({ count: count + 1 });
  };
  
  reset = () => {
    this.setState({ count: 0 });
  }
}