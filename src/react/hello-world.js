﻿/* global React */

export default class HelloWorld extends React.PureComponent {
  render() {
    return <div>
      Hello World
    </div>;
  }
}