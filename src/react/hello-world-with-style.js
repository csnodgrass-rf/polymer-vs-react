﻿/* global React */

export default class HelloWorldWithStyle extends React.PureComponent {
  render() {
    return <React.Fragment>
      <style>{`
        div#HelloWorldWithStyle {
          background: #EEE;
          border: 1px solid #36F;
          padding: 5px 10px;
          font-size: 20px;
          line-height: 1;
        }
      `}</style>
      <div id="HelloWorldWithStyle">Hello World</div>
    </React.Fragment>;
  }
}