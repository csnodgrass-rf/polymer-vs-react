﻿import { join } from 'path';
import express from 'express';
import nodeModuleCompile from './node-module-compile';

const ROOT = join(__dirname, '..');
const PUBLIC = join(__dirname, '../dist');

const PORT = process.env.PORT || 9000;

const app = express();

app.use('/node_modules', nodeModuleCompile(join(ROOT, 'node_modules')));
app.use('/public', express.static(PUBLIC));

app.get('*', (req, res, next) => {
  if (/^\/(node_modules|public)/.test(req.path)) {
    next();
  } else {
    res.sendFile(join(ROOT, 'index.html'), () => next());
  }
});

app.get('*', (req, res) => {
  res.status(404).end();
});

app.listen(
  PORT,
  () => console.log(`Listening on port ${PORT}`)
);