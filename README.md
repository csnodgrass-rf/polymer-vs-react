﻿# Polymer vs React

This project contains identical sample components written in Polymer and React for side-by-side comparison.

## Running

    npm install
    npm start
    
 It will be available in your browser on localhost:9000
 
 ## Notes
 
  - React was used for the views, app and page because Polymer will play nicer with React than vice versa,
    since Polymer elements are just regular HTML elements.